 

package com.inspur.edp.cef.spi.scope;


import com.inspur.edp.cef.api.scope.ICefScopeNodeParameter;

public abstract class AbstractCefScopeNodeParameter implements ICefScopeNodeParameter {

  public abstract String getParameterType();

  public boolean isMergableCrossType() {
    return false;
  }
}
