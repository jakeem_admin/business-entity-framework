 

package com.inspur.edp.cef.spi.scope;


import com.inspur.edp.cef.api.scope.ICefScopeNodeParameter;
import java.util.List;

public abstract class AbstractCefScopeExtension {

  public abstract void onSetComplete();

  private List privateParameters;

  public List<ICefScopeNodeParameter> getParameters() {
    return privateParameters;
  }

  protected final <T> List<T> getParamsGeneric(){
  	return (List<T>) getParameters();
	}

  public void setParameters(java.util.List<ICefScopeNodeParameter> value) {
    privateParameters = value;
  }
}
