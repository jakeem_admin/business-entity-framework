 

package com.inspur.edp.commonmodel.spi;

import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataSerItem;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntityChangeSerializer;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCMEntityChangeJsonSerializer extends AbstractEntityChangeSerializer {
  public AbstractCMEntityChangeJsonSerializer(
      String nodeCode, boolean isRoot, List<AbstractEntitySerializerItem> serializers) {
    super(nodeCode, isRoot, serializers);
  }
}
