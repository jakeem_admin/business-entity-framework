

package com.inspur.edp.commonmodel.spi;


import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataSerItem;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntityChangeJsonDeSerializer;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;
import com.inspur.edp.commonmodel.api.ICMManager;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCMEntityChangeJsonDeSerializer extends
		AbstractEntityChangeJsonDeSerializer
{
	public AbstractCMEntityChangeJsonDeSerializer(String nodeCode, boolean isRoot, List<AbstractEntitySerializerItem> serializers)
	{
		super(nodeCode, isRoot, serializers);
	}

//	private  static List<AbstractEntitySerializerItem> getEntitySeriaItems(List<AbstractEntitySerializerItem> serializers)
//	{
//		ArrayList<AbstractEntitySerializerItem> list =new ArrayList<>();
//		if(serializers==null)
//			return list;
//		for (AbstractEntitySerializerItem item:serializers)
//		{
//			list.add(item);
//		}
//		return list;
//	}

	@Override
	protected final IEntityData createData()
	{
		if (isRoot)
		{
			return getCMManager().createUnlistenableData("");
		}
		return getCMManager().createUnlistenableChildData(nodeCode, "");
	}

	protected abstract ICMManager getCMManager();
}
