/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.delete;

import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.bef.core.lock.LockUtils;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;

public class DeleteMgrAction extends AbstractManagerAction<VoidActionResult> {
  private String dataId;

  public DeleteMgrAction(IBEManagerContext managerContext, String dataId) {
    super(managerContext);
    this.dataId = dataId;
  }

  @Override
  public final void execute() {
    getBEManagerContext().checkAuthority("Delete");
//    AuthorityUtil.checkAuthority("Delete");
//    FuncSessionManager.getCurrentSession().getBefContext().setCurrentOperationType("Delete");
    IBusinessEntity be = getBEManagerContext().getEntity(dataId);
    RetrieveParam tempVar = new RetrieveParam();
    tempVar.setNeedLock(true);
RespectiveRetrieveResult retrieveRez = be.retrieve(tempVar);
    LockUtils.checkLocked(be.getBEContext());
    be.delete();
  }

  @Override
  protected final IMgrActionAssembler getMgrAssembler() {
    return getMgrActionAssemblerFactory().getDeleteMgrActionAssembler(getBEManagerContext());
  }
}
