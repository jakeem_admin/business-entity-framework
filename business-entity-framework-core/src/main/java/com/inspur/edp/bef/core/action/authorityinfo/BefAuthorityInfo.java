/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.authorityinfo;

import com.inspur.edp.cef.api.RefObject;
import java.util.ArrayList;
import java.util.List;

public class BefAuthorityInfo {
private String actionCode;
private List<String[]> valuesList=new ArrayList<>();

  public BefAuthorityInfo(String actionCode) {
    this.actionCode = actionCode;
  }

  public boolean hasValues(String[] values)
  {
    for (String[] valuesItem :valuesList)
    {
      if(itemsEquals(valuesItem,values)) {
        return true;
      }
    }
    return false;
  }

  private boolean itemsEquals(String[] valuesItem, String[] values) {
    if(valuesItem==null||valuesItem.length==0)
    {
      if(values==null||values.length==0)
        return true;
      else
        return false;
    }

    if(valuesItem.length!=values.length)
      return false;

    for(int i =0;i<valuesItem.length;i++)
    {
      if(compareString(valuesItem[i],values[i]))
        continue;
      return false;
    }
    return true;
  }

  private boolean compareString(String s, String value) {
    if(s==null||s.isEmpty())
      return value==null||value.isEmpty();
    return s.equals(value);
  }

  public String getActionCode() {
    return actionCode;
  }

  public void addList(String[] values)
  {
    valuesList.add(values);
  }
}
