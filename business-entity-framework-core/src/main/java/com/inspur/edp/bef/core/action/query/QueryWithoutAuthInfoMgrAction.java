/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.query;

import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import java.util.ArrayList;

public class QueryWithoutAuthInfoMgrAction extends QueryManagerAction {
    private EntityFilter filter;
    private String nodeCode;

    public QueryWithoutAuthInfoMgrAction(IBEManagerContext context, String nodeCode,
        EntityFilter filter) {
        super(context, nodeCode, filter);
    }


    @Override public void execute() {

        super.execute();
    }
    protected ArrayList<AuthorityInfo> getAuthorityInfosWithQuery(IBEManagerContext ibeManagerContext){
        return  null;
    }
}
