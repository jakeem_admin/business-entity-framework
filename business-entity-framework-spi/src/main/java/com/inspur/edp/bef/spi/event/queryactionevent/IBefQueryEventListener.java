/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.event.queryactionevent;

import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;


import java.util.ArrayList;
import java.util.List;

public interface IBefQueryEventListener {
    public void beforeQuery(IBEManager manager, EntityFilter filter);
    //查询前联动计算
    public void beforeTotalCXQDeterminate(EntityFilter filter, EntityFilter customfilter, EntityFilter totalFilter);
    public void beforeCXQDeterminate(EntityFilter beforefilter);
    public void processRecordClassPath(AbstractDeterminationAction determination);
    public void afterCXQDeterminate(EntityFilter afterfilter);
    public void afterTotalCXQDeterminate(EntityFilter afterfilter, int TotalCount);
    //查询
    public void beforeCoreQuery(EntityFilter finalfilter);
    public void processGetAuthority(ArrayList<AuthorityInfo> auls);
    public void afterCoreQuery(IBEManager bemanager, List<IEntityData> resultvar);
    //查询后联动计算
    public void beforeTotalCXHDeterminate(IBEManager bemanager,List<IEntityData> beforedatas);
    public void beforeDataCXHDeterminate(IBEManager bemanager,ICefDeterminationContext context);
    public void afterDataCXHDeterminate(IBEManager bemanager,ICefDeterminationContext context, int TotalCount);
    public void beforeCXHDeterminate(IBEManager bemanager, ICefDeterminationContext context);
    public void afterCXHDeterminate(IBEManager bemanager, ICefDeterminationContext context);
    public void afterTotalCXHDeterminate(IBEManager bemanager, List<IEntityData> resultvar, int TotalCount);


    //查询后
    public void afterQuery();

    public void queryUnnormalStop(Exception e);
}
