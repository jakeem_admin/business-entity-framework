/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.changeset;

import com.inspur.edp.cef.entity.entity.IKey;
import java.util.*;


public class ModifyChangeDetail extends AbstractModifyChangeDetail implements IKey
{
	private String id;
	public final String getID()
	{
		return id;
	}
	public final void setID(String value)
	{
		id = value;
	}


	 public String getDataID()
	 {return getID();}

	 public void setDataID(String value)
	 {setID(value);}

	public ModifyChangeDetail(String id)
	{
		setID(id);
	}

	private HashMap<String, Map<String, IChangeDetail>> childChanges;
	public Map<String, Map<String, IChangeDetail>> getChildChanges() {
		return (childChanges != null) ? childChanges : (childChanges = new HashMap<String, Map<String, IChangeDetail>>());
	}

	public final void addChildChangeSet(String childCode, IChangeDetail childChangeDetail)
	{
		Map<String, IChangeDetail> childChange = null;
		childChange = getChildChanges().get(childCode);
		if (childChange == null)
		{
			getChildChanges().put(childCode, childChange = new LinkedHashMap<String, IChangeDetail>());
		}
		childChange.put(childChangeDetail.getDataID(), childChangeDetail);
	}

	public final void removeChildChangeSet(String nodeCode, String id)
	{
		throw new UnsupportedOperationException();
	}

	public final IChangeDetail getChildChangeDetail(String childCode, String id)
	{
		if (getChildChanges() == null)
		{
			return null;
		}
		Map<String, IChangeDetail> childChange = getChildChanges().get(childCode);
		if (childChange == null || childChange.isEmpty())
		{
			return null;
		}
		if(childChange.containsKey(id))
			return childChange.get(id);
		return null;
	}

	@Override
	public ModifyChangeDetail putItem(String key, Object value) {
		return (ModifyChangeDetail)super.putItem(key, value);
	}

	@Override
	public ModifyChangeDetail putItem(String key, String nestedKey, Object value) {
		return (ModifyChangeDetail)super.putItem(key, nestedKey, value);
	}

	public 	IChangeDetail clone()
	{
		return InnerUtil.cloneModifyChange(this);

	}
}
