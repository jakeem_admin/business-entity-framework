/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.accessor.entity;

import com.inspur.edp.cef.entity.accessor.base.AccessorBase;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.changeset.AbstractModifyChangeDetail;
import com.inspur.edp.cef.entity.changeset.AddChangeDetail;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import java.util.Map;

public abstract class EntityAccessor extends AccessorBase implements IEntityData
{
	protected EntityAccessor(IEntityData data)
	{
		super(data);
	}


	//[JsonIgnore]
	@Override
	public  IEntityData getInnerData()
	{
	 	return (IEntityData)super.getInnerData();
	}
	public void setInnerData(IEntityData value)
	 {
	 	super.setInnerData(value);
	 }


	//[JsonIgnore]
		public String getID()
		{
			return getInnerData().getID();
		}

	public abstract String getNodeCode();

	public abstract java.util.HashMap<String, IEntityDataCollection> getChilds();

	public abstract ICefData createChild(String childCode);

	public void mergeChildData(String nodeCode, java.util.ArrayList<IEntityData> childData)
	{
		throw new RuntimeException("Accessor不支持MergeChildData");
	}
	public abstract IEntityDataCollection createAndSetChildCollection(String childCode);
}
