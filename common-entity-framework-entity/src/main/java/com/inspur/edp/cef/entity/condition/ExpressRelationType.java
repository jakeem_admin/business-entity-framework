/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

/** 
 表达式关系符的枚举
 
*/
public enum ExpressRelationType
{
	Empty(0),
	And(1),
	Or(2);

	private int intValue;
	private static java.util.HashMap<Integer, ExpressRelationType> mappings;
	private synchronized static java.util.HashMap<Integer, ExpressRelationType> getMappings()
	{
		if (mappings == null)
		{
			mappings = new java.util.HashMap<Integer, ExpressRelationType>();
		}
		return mappings;
	}

	private ExpressRelationType(int value)
	{
		intValue = value;
		ExpressRelationType.getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static ExpressRelationType forValue(int value)
	{
		return getMappings().get(value);
	}
}
