/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

//TODO Processer中增加比较符的校验
/** 
 表达式比较符类型的枚举
 
*/
public enum ExpressCompareType
{
	Equal(0),
	NotEqual(1),
	Greater(2),
	GreaterOrEqual(3),
	Less(4),
	LessOrEqual(5),
	Like(6),
	LikeStartWith(7),
	LikeEndWith(8),
	NotLike(9),
	NotLikeStartWith(10),
	NotLikeEndWith(11),
	Is(12),
	IsNot(13),
	In(14),
	NotIn(15),
	LikeIgnoreCase(16);

	private int intValue;
	private static java.util.HashMap<Integer, ExpressCompareType> mappings;
	private synchronized static java.util.HashMap<Integer, ExpressCompareType> getMappings()
	{
		if (mappings == null)
		{
			mappings = new java.util.HashMap<Integer, ExpressCompareType>();
		}
		return mappings;
	}

	private ExpressCompareType(int value)
	{
		intValue = value;
		ExpressCompareType.getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static ExpressCompareType forValue(int value)
	{
		return getMappings().get(value);
	}
}
