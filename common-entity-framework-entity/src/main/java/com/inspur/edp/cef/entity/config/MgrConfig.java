/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.config;

public class MgrConfig
{
	private String privateAssembly;
	public final String getAssembly()
	{
		return privateAssembly;
	}
	public final void setAssembly(String value)
	{
		privateAssembly = value;
	}
	private String privateClass;
	public final String getClassName()
	{
		return privateClass;
	}
	public final void setClass(String value)
	{
		privateClass = value;
	}
	private String privateImpAssembly;
	public final String getImpAssembly()
	{
		return privateImpAssembly;
	}
	public final void setImpAssembly(String value)
	{
		privateImpAssembly = value;
	}
	private String privateImpClass;
	public final String getImpClass()
	{
		return privateImpClass;
	}
	public final void setImpClass(String value)
	{
		privateImpClass = value;
	}
	private String privateAllInterfaceAssembly;
	public final String getAllInterfaceAssembly()
	{
		return privateAllInterfaceAssembly;
	}
	public final void setAllInterfaceAssembly(String value)
	{
		privateAllInterfaceAssembly = value;
	}
	private String privateAllInterfaceClassName;
	public final String getAllInterfaceClassName()
	{
		return privateAllInterfaceClassName;
	}
	public final void setAllInterfaceClassName(String value)
	{
		privateAllInterfaceClassName = value;
	}
}
