/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.action.determination;

import com.inspur.edp.cef.entity.exception.CircleDeterminationsFound;
import java.util.LinkedList;

public class DtmExecutionHistory {
  public static final int groupMaxCapacity = 99;
  // private const int groupStartCapacity = 15;
  // private const int itemStartCapapcity = 8;
  // TODO:string描述的信息太少将来改成实体类
  private java.util.LinkedList<java.util.Collection> history =
      new java.util.LinkedList<java.util.Collection>();

  public final void beginGroup(String groupName) {
    // DataValidator.CheckForEmptyString(groupName, "dtmId");

    if (history.size() >= groupMaxCapacity) {
      throw new CircleDeterminationsFound(getExecutiongHistory());
    }
    history.addLast(new LinkedList<String>());
  }

  public final String getExecutiongHistory() {
    // TODO:暂不实现
    return "暂不支持显示执行顺序";
    // if (history.Count <= 0)
    //    return "";
    // StringBuilder sb = new StringBuilder();
    // foreach (var item in history)
    // {
    //    sb.Append(item).Append(", ");
    // }
    // return sb.ToString();
  }

  public final void addGroupItem(String itemId) {
    history.getLast().add(itemId);
  }
}
