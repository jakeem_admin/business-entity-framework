/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.readerwriter;

import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;

import java.sql.ResultSet;
import java.sql.SQLException;


public class CefDataReader implements ICefReader {
    private Object[] reader;
    private java.util.HashMap<String, Integer> propDBIndexMapping;
    private boolean usePropDBIndexMapping = false;

//    public CefDataReader(Object[] reader) {
//        this.reader = reader;
//    }

    public CefDataReader(Object[] reader, java.util.HashMap<String, Integer> propDBIndexMapping) {
        this.reader = reader;
        usePropDBIndexMapping = true;
        this.propDBIndexMapping = propDBIndexMapping;
    }

    public final Object readValue(String propName) {
        try{
            if (usePropDBIndexMapping&&propDBIndexMapping.containsKey(propName)) {
                return reader[propDBIndexMapping.get(propName)];// (propDBIndexMapping.get(propName));
            }
        }catch(Exception e){
            String errorMsg = "获取属性"+propName+"失败";
            if (usePropDBIndexMapping&&propDBIndexMapping.containsKey(propName)) {
                errorMsg += ",当前索引" + propDBIndexMapping.get(propName);
            }
            throw new RuntimeException(errorMsg,e);
        }

       return null;
    }

    @Override
    public boolean hasProperty(String propName) {
        return propDBIndexMapping.containsKey(propName);
    }

//	public final Object getString(String propName) throws SQLException
//	{
//		if (usePropDBIndexMapping)
//		{
//			return reader.getString(propDBIndexMapping.get(propName));
//		}
//		return reader.getString(propName);
//	}
}
