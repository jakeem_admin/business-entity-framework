/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.dbcolumninfo;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class DbColumnInfoCollection implements Iterable<DbColumnInfo> {
    private DbColumnInfo primaryKey;
    private DbColumnInfo parentIdColumn;
    private LinkedHashMap<String, DbColumnInfo> dict = new LinkedHashMap<String, DbColumnInfo>();
    //不存储关联带出字段
    private LinkedHashMap<String, DbColumnInfo> baseDict = new LinkedHashMap<String, DbColumnInfo>();

    public LinkedHashMap<String, DbColumnInfo> getBaseDict() {
        return baseDict;
    }

    public final DbColumnInfo getPrimaryKey() {
        return primaryKey;
    }

    public final void setPrimaryKey(DbColumnInfo value) {
        primaryKey = value;
    }

    public final DbColumnInfo getParentIdColumn() {
        return parentIdColumn;
    }

    public final void setParentIdColumn(DbColumnInfo value) {
        parentIdColumn = value;
    }

    public int getCount() {
        return dict.size();
    }

    public final void add(DbColumnInfo info) {
        if (info.getIsPrimaryKey()) {
            if (getPrimaryKey() == null || info.getDbColumnName().equals(getPrimaryKey().getDbColumnName())) {
                setPrimaryKey(info);
            } else {
                throw new RuntimeException("请勿添加重复主键");
            }
        }

        if (info.getIsParentId()) {
            if (getParentIdColumn() == null || info.getDbColumnName().equals(getParentIdColumn().getDbColumnName())) {
                setParentIdColumn(info);
            } else {
                throw new RuntimeException("请勿添加重复父节点ID字段");
            }
        }
        synchronized (dict){
            dict.put(info.getColumnName(), info);
            if(!info.getIsAssociateRefElement() && !baseDict.containsKey(info.getColumnName())){
                baseDict.put(info.getColumnName(), info);
            }
        }
    }

    public final void addRange(DbColumnInfoCollection infos) {
        if (infos == null || infos.getCount() < 1) {
            return;
        }
        for (DbColumnInfo info : infos) {
            Object tempVar = info.clone();
            add((DbColumnInfo) ((tempVar instanceof DbColumnInfo) ? tempVar : null));
        }
    }

    public DbColumnInfo getItem(String key) {
        return getColumnInfo(key);
    }

    private DbColumnInfo getColumnInfo(String key) {
        if (dict.containsKey(key)) {
            return dict.get(key);
        }
        for (Map.Entry<String, DbColumnInfo> columnInfo : dict.entrySet()) {
            if (columnInfo.getKey().equalsIgnoreCase(key)) {
                return columnInfo.getValue();
            }
        }

        return null;
    }

    public final boolean contains(String columnName) {
        synchronized (dict){
            if (dict.containsKey(columnName)) {
                return true;
            }
            Iterator iterator = dict.entrySet().iterator();
            while (iterator.hasNext()){
                if (((Map.Entry<String, DbColumnInfo>)iterator.next()).getKey().equalsIgnoreCase(columnName)) {
                    return true;
                }
            }
            return false;
        }
    }

    @Override
    public Iterator<DbColumnInfo> iterator() {
        return dict.values().iterator();
    }

    public final boolean hasMultiLanguage()
    {
            for (DbColumnInfo col : this) {
                if (col.getIsMultiLang()) {
                    return true;
                }
            }
            return false;
    }
}
