/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.typetransprocesser;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;

import io.netty.handler.codec.compression.FastLzFrameEncoder;
import java.sql.Connection;

public class Bool2CharProcesser implements ITypeTransProcesser
{
	private static Bool2CharProcesser instance;
	public static Bool2CharProcesser getInstacne()
	{
		if (instance == null)
		{
			instance = new Bool2CharProcesser();
		}
		return instance;
	}
	private Bool2CharProcesser()
	{

	}

	public final Object transType(FilterCondition filter, Connection db)
	{
		if (filter.getValue().toUpperCase().equals("FALSE") || filter.getValue().equals("0"))
		{
			filter.setValue("0");
		}
		else if (filter.getValue().toUpperCase().equals("TRUE") || filter.getValue().equals("1"))
		{
			filter.setValue("1");
		}
		else
		{
			throw new RuntimeException("过滤条件中写入了错误格式的布尔类型数据");
		}
		return filter.getValue();
	}

	public final Object transType(Object value)
	{
		return this.transType(value, true);
	}

	@Override
	public Object transType(Object value, boolean isNull) {
		if(value == null){
			if(!isNull){
				return "0";
			}
			return null;
		}
		if(!(value instanceof Boolean))
		{
			throw new RuntimeException("传入了错误的类型:"+value.getClass().getName());
		}
		if (((Boolean)value).booleanValue())
		{
			return "1";
		}
		return "0";
	}
	}
