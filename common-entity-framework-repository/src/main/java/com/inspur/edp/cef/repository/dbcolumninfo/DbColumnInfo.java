/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.dbcolumninfo;

import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.repository.adaptor.KeyWordsManager;
import com.inspur.edp.cef.repository.adaptoritem.BaseAdaptorItem;
import com.inspur.edp.cef.repository.adaptoritem.dbprocessor.DbProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.AbstractRepoPrcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.AssociationRepoProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.BigDecimalRepoProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.BlobRepoProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.BooleanRepoProccessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.ClobRepoProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.ComplexUdtRepoProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.DateTimeRepoProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.EnumRepoDbProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.IntRepoProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.SimpleAssoUdtRepoProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.SimpleEnumUdtRepoProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.SimpleUdtRepoProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.StringRepoProcessor;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ComplexUdtPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ObjectType;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.SimpleAssoUdtPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.SimpleEnumUdtPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.SimpleUdtPropertyInfo;

public class DbColumnInfo implements Cloneable {
    private String columnName;
    private String dbColumnName;
    private GspDbDataType columnType;
    private int length;
    private int precision;
    private Object defaultValue;
    private boolean isPrimaryKey;
    private boolean isAssociateRefElement;
    private boolean isMultiLang;
    private boolean isParentId;
    private boolean isUdtElement;
    private boolean isAssociation;
    private boolean isEnum;
    private AbstractRepoPrcessor repoPrcessor;
    private DataTypePropertyInfo dataTypePropertyInfo;
    /**
     * udt字段，关联带出字段所属的字段标签
     */
    private String belongElementLabel;
    //public Func<FilterCondition, IGSPDatabase, object> typetransprocesser { get; set; }
    private ITypeTransProcesser typeTransProcesser;
    private boolean isRef;
    public DbColumnInfo()
    {}

    DbColumnInfo(String columnName,String dbColumnName,GspDbDataType columnType,int length,int precision,boolean isPrimaryKey,boolean isAssociateRefElement,boolean isMultiLang,boolean isParentId,boolean isUdtElement
    ,boolean isAssociation,boolean isEnum,String belongElementLabel,ITypeTransProcesser typeTransProcesser)
    {

      this.columnName = columnName;
      this.dbColumnName = dbColumnName;
      this.columnType = columnType;
      this.length = length;
      this.precision = precision;
      this.isPrimaryKey = isPrimaryKey;
      this.isAssociateRefElement = isAssociateRefElement;
      this.isMultiLang = isMultiLang;
      this.isParentId = isParentId;
      this.isUdtElement = isUdtElement;
      this.isAssociation = isAssociation;
      this.isEnum = isEnum;
      this.belongElementLabel = belongElementLabel;
      this.typeTransProcesser = typeTransProcesser;
    }

    public final String getColumnName() {
        return columnName;
    }

    public final String getAliasName(int index){
        if(columnName.length()>=30)
        {
            return columnName.substring(0, 26) + index;
        }
        else{
            return KeyWordsManager.getColumnAlias(columnName);
        }
    }

    public final void setColumnName(String value) {
        columnName = value;
    }

    public final String getDbColumnName() {
        return dbColumnName;
    }

    public final void setDbColumnName(String value) {
        dbColumnName = value;
    }

    public final GspDbDataType getColumnType() {
        return columnType;
    }

    public final void setColumnType(GspDbDataType value) {
        columnType = value;
    }

    public final int getLength() {
        return length;
    }

    public final void setLength(int value) {
        length = value;
    }

    public final int getPrecision() {
        return precision;
    }

    public final void setPrecision(int value) {
        precision = value;
    }

    public final Object getDefaultValue() {
        return defaultValue;
    }

    public final void setDefaultValue(Object value) {
        defaultValue = value;
    }

    public final boolean getIsPrimaryKey() {
        return isPrimaryKey;
    }

    public final void setIsPrimaryKey(boolean value) {
        isPrimaryKey = value;
    }

    public final boolean getIsAssociateRefElement() {
        return isAssociateRefElement;
    }

    public final void setIsAssociateRefElement(boolean value) {
        isAssociateRefElement = value;
    }

    public final boolean getIsMultiLang() {
        return isMultiLang;
    }

    public final void setIsMultiLang(boolean value) {
        isMultiLang = value;
    }

    public final boolean getIsParentId() {
        return isParentId;
    }

    public final void setIsParentId(boolean value) {
        isParentId = value;
    }

    public final boolean getIsUdtElement() {
        return isUdtElement;
    }

    public final void setIsUdtElement(boolean value) {
        isUdtElement = value;
    }

    public final boolean getIsAssociation() {
        return isAssociation;
    }

    public final void setIsAssociation(boolean value) {
        isAssociation = value;
    }

    public final boolean getIsEnum() {
        return isEnum;
    }

    public final void setIsEnum(boolean value) {
        isEnum = value;
    }

    public final String getBelongElementLabel() {
        return belongElementLabel;
    }

    public final void setBelongElementLabel(String value) {
        belongElementLabel = value;
    }

    public final ITypeTransProcesser getTypeTransProcesser() {
        return typeTransProcesser;
    }

    public final void setTypeTransProcesser(ITypeTransProcesser value) {
        typeTransProcesser = value;
    }

    public final boolean isRef() {
        return isRef;
    }

    public final void setRef(boolean ref) {
        isRef = ref;
    }

    public final String getRealColumnName() {
        if (getIsMultiLang())
            return getDbColumnName() + "@Language@";
        return getDbColumnName();
    }

    public final Object clone() {
        DbColumnInfo columnInfo = new DbColumnInfo();
        columnInfo.columnName = columnName;
        columnInfo.dbColumnName = dbColumnName;
        columnInfo.columnType = columnType;
        columnInfo.length = length;
        columnInfo.precision = precision;
        columnInfo.defaultValue = defaultValue;
        columnInfo.isPrimaryKey = isPrimaryKey;
        columnInfo.isAssociateRefElement = isAssociateRefElement;
        columnInfo.isMultiLang = isMultiLang;
        columnInfo.isParentId = isParentId;
        columnInfo.isUdtElement = isUdtElement;
        columnInfo.isAssociation = isAssociation;
        columnInfo.isEnum = isEnum;
        columnInfo.belongElementLabel = belongElementLabel;
        columnInfo.typeTransProcesser = typeTransProcesser;
        columnInfo.isRef = isRef;
        return columnInfo;
    }

    public boolean isUdtRefColumn()
    {return false;}


    private AbstractRepoPrcessor getRepoPrcessor()
    {
        if(repoPrcessor!=null)
            return repoPrcessor;
        if(dataTypePropertyInfo.getObjectInfo() instanceof SimpleAssoUdtPropertyInfo)
            repoPrcessor=new SimpleAssoUdtRepoProcessor();
        else if(dataTypePropertyInfo .getObjectInfo() instanceof SimpleEnumUdtPropertyInfo)
            repoPrcessor=new SimpleEnumUdtRepoProcessor();
        else if(dataTypePropertyInfo .getObjectInfo() instanceof ComplexUdtPropertyInfo)
            repoPrcessor=new ComplexUdtRepoProcessor();
        else if(dataTypePropertyInfo.getObjectInfo() instanceof SimpleUdtPropertyInfo)
            repoPrcessor=new SimpleUdtRepoProcessor();
        else
        {
            if(dataTypePropertyInfo.getObjectType()== ObjectType.Association)
                repoPrcessor=new AssociationRepoProcessor();
            else if(dataTypePropertyInfo.getObjectType()==ObjectType.Enum)
                repoPrcessor=new EnumRepoDbProcessor();
            else
            {
                switch (dataTypePropertyInfo.getFieldType())
                {

                    case String:
                        repoPrcessor=new StringRepoProcessor();
                        break;
                    case Text:
                        repoPrcessor=new ClobRepoProcessor();
                        break;
                    case Integer:
                        repoPrcessor=new IntRepoProcessor();
                        break;
                    case Decimal:
                        repoPrcessor=new BigDecimalRepoProcessor();
                        break;
                    case Boolean:
                        repoPrcessor=new BooleanRepoProccessor();
                        break;
                    case Date:
                    case DateTime:
                        repoPrcessor=new DateTimeRepoProcessor();
                        break;
                    case Binary:
                        repoPrcessor=new BlobRepoProcessor();
                        break;
                    case Udt:
                       throw new RuntimeException();
                }
            }
        }
        return repoPrcessor;
    }

    public final Object readProperty(ICefReader reader, BaseAdaptorItem baseAdaptorItem,
        DbProcessor dbProcessor)
    {
        Object obj = null;
        try{
            obj = getRepoPrcessor().readProperty(this,dbProcessor,baseAdaptorItem,reader);
        }
        catch (Exception ex){
            throw new RuntimeException("读取属性["+this.getColumnName()+"]的值失败", ex);
        }
        return obj;
    }

    public final Object getPropertyChangeValue(Object value)
    {
        return getRepoPrcessor().getPropertyChangeValue(this,value);
    }

    public final  Object getPersistenceValue(Object value)
    {
        return getRepoPrcessor().getPersistenceValue(this,value);
    }

    public DataTypePropertyInfo getDataTypePropertyInfo() {
        return dataTypePropertyInfo;
    }

    public void setDataTypePropertyInfo(
        DataTypePropertyInfo dataTypePropertyInfo) {
        this.dataTypePropertyInfo = dataTypePropertyInfo;
    }
}
