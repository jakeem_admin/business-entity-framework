/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler;

import com.inspur.edp.cef.api.repository.IRootRepository;

import java.util.ArrayList;
import java.util.HashMap;

public class AssociationInfo
{
	private String privateConfigId;
	public final String getConfigId()
	{
		return privateConfigId;
	}
	public final void setConfigId(String value)
	{
		privateConfigId = value;
	}
	/** 
	 关联实体节点名称（默认应该是主节点）？？有没有必要加
	 
	*/
	private String privateNodeCode;
	public final String getNodeCode()
	{
		return privateNodeCode;
	}
	public final void setNodeCode(String value)
	{
		privateNodeCode = value;
	}

	/** 
	 关联引用字段集合
	 
	 Key是本实体上关联引用字段的编号
	 Value是引用实体上字段的编号
	*/
	private java.util.HashMap<String, String> privateRefColumns;
	public final java.util.HashMap<String, String> getRefColumns()
	{
		return privateRefColumns;
	}
	public final void setRefColumns(java.util.HashMap<String, String> value)
	{
		privateRefColumns = value;
	}

	/** 
	 关联实体仓库
	 
	*/
	private IRootRepository privateRefRepository;
	public final IRootRepository getRefRepository()
	{
		return privateRefRepository;
	}
	public final void setRefRepository(IRootRepository value)
	{
		privateRefRepository = value;
	}

	private java.lang.Class privateAssociationType;
	public final java.lang.Class getAssociationType()
	{
		return privateAssociationType;
	}
	public final void setAssociationType(java.lang.Class value)
	{
		privateAssociationType = value;
	}

	/** 
	 源字段名称（当前实体字段）
	 
	*/
	private String privateSourceColumn;
	public final String getSourceColumn()
	{
		return privateSourceColumn;
	}
	public final void setSourceColumn(String value)
	{
		privateSourceColumn = value;
	}

	/** 
	 目标字段名称（关联对应字段）
	 
	*/
	private String privateTargetColumn;
	public final String getTargetColumn()
	{
		return privateTargetColumn;
	}
	public final void setTargetColumn(String value)
	{
		privateTargetColumn = value;
	}

	public ArrayList<AssoCondition> getAssoConditions() {
		return assoConditions;
	}

	public void setAssoConditions(ArrayList<AssoCondition> assoConditions) {
		this.assoConditions = assoConditions;
	}

	private ArrayList<AssoCondition> assoConditions;

	public String getWhere() {
		return where;
	}

	public void setWhere(String where) {
		this.where = where;
	}

	private String where;

	public HashMap<String, String> getAssDbMapping() {
		return getRefColumns();
	}
}
