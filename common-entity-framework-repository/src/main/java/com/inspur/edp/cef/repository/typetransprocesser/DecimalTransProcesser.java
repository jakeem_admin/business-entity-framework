/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.typetransprocesser;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;

import java.math.BigDecimal;
import java.sql.Connection;


public class DecimalTransProcesser implements ITypeTransProcesser {

    private static DecimalTransProcesser instance;

    public static DecimalTransProcesser getInstacne() {
        if (instance == null) {
            instance = new DecimalTransProcesser();
        }
        return instance;
    }

    private DecimalTransProcesser() {

    }

    public final Object transType(FilterCondition filter, Connection db) {
        //return Double.parseDouble(filter.getValue());
        return new BigDecimal(filter.getValue());
    }

    public final Object transType(Object value) {
        return this.transType(value, true);
    }

    @Override
    public Object transType(Object value, boolean isNull) {
        if (value == null){
            if(!isNull){
                return new BigDecimal("0");
            }
            return null;
        }
        if (value instanceof BigDecimal) {
            return value;
        }
        if (value instanceof String) {
            return new BigDecimal((String) value);
        }
        else {
            throw new RuntimeException("浮点类型转换异常，请使用'BigDecimal'或'String'类型");
        }
    }
}
