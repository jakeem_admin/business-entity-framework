/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptor;

import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.api.repository.adaptor.IEntityAdapterContext;
import com.inspur.edp.cef.repository.utils.FilterUtil;

public class EntityRelationalAdapterContext implements IEntityAdapterContext {
    @Override
    public DbParameter buildParam(String paramName, GspDbDataType dataType, Object paramValue) {
        return FilterUtil.buildParam(paramName,dataType,paramValue);
    }
}
