/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem.dbprocessor;

import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.entity.entity.EntityDataPropertyValueUtils;

import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Date;

public class ShenTongDbProcessor extends DbProcessor{

    @Override
    protected void buildDateTimeParameter(Query query, int i, DbParameter param) {
        if(param.getValue() == null){
            query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
        }
        else {
            if(param.getValue() instanceof java.sql.Date){//fix
                query.setParameter(i, new java.util.Date(((java.sql.Date) param.getValue()).getTime()), TemporalType.DATE);
            }
            else {
                query.setParameter(i, Date.from(((Date)param.getValue()).toInstant()), TemporalType.DATE);
            }
        }
    }

    @Override
    public String getStringValue(ICefReader reader,String propName){
        Object obj=reader.readValue(propName);
        if (obj == null){
            return EntityDataPropertyValueUtils.getStringPropertyDefaultValue();
        }
        else  if(obj.toString().toLowerCase().contains("oracle.sql.clob")){
            return getClobValue(reader, propName);
        }
        return obj.toString();
    }

    @Override
    public byte[] getBlobValue(ICefReader reader,String propName)
    {
        Object obj=reader.readValue(propName);
        if(!(obj instanceof Blob) && obj != null )
        {
            throw new RuntimeException("Oracle数据库取出数据类型异常！");
        }
        else {
            try {
                return obj == null ? EntityDataPropertyValueUtils.getBinaryPropertyDefaultValue() : ((Blob) obj).getBytes(1L, (int) ((Blob) obj).length());
            } catch (SQLException e) {
                throw new RuntimeException("解析备注类型数据：" + obj + "失败");
            }
        }
    }
}
