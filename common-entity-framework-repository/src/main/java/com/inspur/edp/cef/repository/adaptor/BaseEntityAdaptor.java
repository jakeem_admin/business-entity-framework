/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptor;

import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.api.repository.adaptor.IEntityAdaptor;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.repository.DataSaveParameter;
import com.inspur.edp.cef.repository.assembler.AbstractDataAdapterExtendInfo;

import com.inspur.edp.cef.repository.dac.DacSaveContext;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseEntityAdaptor extends DataTypeAdapter implements IEntityAdaptor {
    private ArrayList<AbstractDataAdapterExtendInfo> dataAdapterExtendInfos = new ArrayList<>();

    private Map<String, Object> privatePars;
    private HashMap<String, String> privateVars;

    protected BaseEntityAdaptor() {
    }

    protected BaseEntityAdaptor(boolean init) {
        super(init);
    }

    protected final Map<String, Object> getPars() {
        return privatePars;
    }

    private void setPars(Map<String, Object> value) {
        privatePars = value;
    }

    protected final HashMap<String, String> getVars() {
        return privateVars;
    }

    private void setVars(HashMap<String, String> value) {
        privateVars = value;
    }

    protected Connection getDB() {
        return null;
    }

    public void initParams(Map<String, Object> pars) {
        setPars(pars);
    }

    public void initVars(HashMap<String, String> vars) {
        setVars(vars);
    }

    public ArrayList<AbstractDataAdapterExtendInfo> getDataAdapterExtendInfos() {
        return dataAdapterExtendInfos;
    }

    public void setDataAdapterExtendInfos(ArrayList<AbstractDataAdapterExtendInfo> infos) {
        dataAdapterExtendInfos = infos;
        for (AbstractDataAdapterExtendInfo info:infos) {
            if(info.getAssociationInfos() ==null)
                continue;
            getAssociationInfos().addAll(info.getAssociationInfos());
        }
    }

    //public abstract IEntityAdaptor ParentAdaptor { get; }
    //public abstract IList<IEntityAdaptor> ChildAdaptors { get; }

    public abstract int delete(String id, DataSaveParameter par) throws SQLException;

    public abstract void delete(List<String> ids, DataSaveParameter par) throws SQLException;

    public abstract List<IEntityData> getDataByIDs(List<String> dataIds) throws SQLException;

    public abstract IEntityData getDataByID(String dataIds) throws SQLException;

    public abstract int insert(IEntityData data, DataSaveParameter par, DacSaveContext batcher) throws SQLException;

    public abstract int modify(ModifyChangeDetail change, DataSaveParameter par, DacSaveContext batcher) throws SQLException;

    public abstract List<IEntityData> query(EntityFilter filter, ArrayList<AuthorityInfo> authorities);
}
