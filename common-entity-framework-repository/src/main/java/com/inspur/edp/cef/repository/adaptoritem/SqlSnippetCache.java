/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem;

import com.inspur.edp.cef.repository.adaptoritem.sqlsnippetprocessor.SqlSnippetProcessor;

import java.util.HashMap;
import java.util.Map;

public final class SqlSnippetCache {
    private boolean hasMultiLangCol;

    public SqlSnippetCache()
    {
        hasMultiLangCol = false;
    }

    public SqlSnippetCache(boolean hasMultiLangCol)
    {
        this.hasMultiLangCol = hasMultiLangCol;
    }



    //todo ParentJoin的SQL片段拼接待处理
    private Map<String, String> parentJoinMap = new HashMap<>();
    private Map<String, String> deleteSqlBatchMap = new HashMap<>();
    private Map<String, String> getDataByIdsSqlMap = new HashMap<>();
    private Map<String, String> getDataByIdSqlMap = new HashMap<>();
    private Map<String, String> joinTableNameMap = new HashMap<>();
    private Map<String, String> innerDeleteSqlMap = new HashMap<>();
    private Map<String, String> innerInsertSqlMap = new HashMap<>();
    private Map<String, String> innerModifySqlMap = new HashMap<>();
    private Map<String, SqlSnippetProcessor> stringSqlSnippetProcessorMap=new HashMap<>();
    private String primaryKey;

    public final Map<String, String> getParentJoinMap() {
        return parentJoinMap;
    }

    final Map<String, String> getDeleteSqlBatchMap() {
        return deleteSqlBatchMap;
    }

    final Map<String, String> getGetDataByIdsSqlMap() {
        return getDataByIdsSqlMap;
    }

    final Map<String, String> getGetDataByIdSqlMap() {
        return getDataByIdSqlMap;
    }

    final Map<String, String> getJoinTableNameMap() {
        return joinTableNameMap;
    }

    final Map<String, String> getInnerDeleteSqlMap() {
        return innerDeleteSqlMap;
    }

    final Map<String, String> getInnerInsertSqlMap() {
        return innerInsertSqlMap;
    }

    final Map<String, String> getInnerModifySqlMap() {
        return innerModifySqlMap;
    }

    private  HashMap<String,Integer> propIndexMappingDict;

    public  final HashMap<String, Integer> getPropIndexMappingDict() {
        return propIndexMappingDict;
    }

    public final void setPropIndexMappingDict(
            HashMap<String, Integer> propIndexMappingDict) {
        this.propIndexMappingDict = propIndexMappingDict;
    }

    private AdaptorSqlCache adaptorSqlCache=new AdaptorSqlCache();
    public final AdaptorSqlCache getAdaptorSqlCache() {
        return adaptorSqlCache;
    }

    private AdaptorItemSqlCache adaptorItemSqlCache=new AdaptorItemSqlCache();

    public  final AdaptorItemSqlCache getAdaptorItemSqlCache() {
        return adaptorItemSqlCache;
    }

    public final String getPrimaryKey() {
        return primaryKey;
    }

    public final void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public final  SqlSnippetProcessor getSqlSnippetProcessor(String dbType,EntityRelationalReposAdaptor adaptor)
    {
        if(stringSqlSnippetProcessorMap.containsKey(dbType))
            return stringSqlSnippetProcessorMap.get(dbType);
        SqlSnippetProcessor processor=new SqlSnippetProcessor(adaptor);
        stringSqlSnippetProcessorMap.put(dbType,processor);
        return processor;
    }

    public final boolean isHasMultiLangCol() {
        return hasMultiLangCol;
    }
    public void setHasMultiLangCol(boolean hasMultiLangCol) {
        this.hasMultiLangCol = hasMultiLangCol;
    }
}
