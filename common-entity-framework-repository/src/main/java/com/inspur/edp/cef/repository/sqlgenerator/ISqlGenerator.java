/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.sqlgenerator;
/** 
 扩展数据库类型SQL集
 
*/
public interface ISqlGenerator
{
	/** 
	 查询sql：{0}字段名称，{1}表名
	 <see cref="string"/>
	 
	*/
	String getQuerySql();

	/** 
	 插入sql：{0}表名，{1}字段名，{2}参数列表
	 <see cref="string"/>
	 
	*/
	String getInsertSql();

	/** 
	 删除sql：{0}表名，{1}别名，{2}id字段名称，{3}id字段值
	 <see cref="string"/>
	 
	*/
	String getDeleteSql();

	/** 
	 更新sql：{0}表名(无别名)，{1}ValueList，{2}别名，{3}id字段名称，{4}id字段值
	 <see cref="string"/>
	 
	*/
	String getUpdateSql();

	/** 
	 根据rootid集合查询数据
	 {0}当前表字段名，{1}RootColumnName，{2}当前表表名，{3}Join结构，{4}rootid集合（{0}）
	 <see cref="string"/>
	 
	*/
	String getSearchSqlByRootIds();

	/** 
	 根据rootid查询数据
	 {0}当前表字段名，{1}RootColumnName，{2}当前表表名，{3}Join结构，{4}rootid（{0}）
	 <see cref="string"/>
	 
	*/
	String getSearchSqlByRootId();

	/** 
	 根据rootid删除数据
	 {0}当前表表名(不带别名), {1}当前表别名，{2}父表表名，{3}表关系, {4}RootColumnName, {5}rootid（{0}）
	 <see cref="string"/>
	 
	*/
	String getDeleteByRoot();

	/** 
	 批量Retrieve数据
	 {0}列名，{1}表名，{2}ID字段名，{3}value
	 <see cref="string"/>
	 
	*/
	String getRetrieveBatchSql();

	/** 
	 单条Retrieve数据
	 {0}列名，{1}表名，{2}ID字段名，{3}value
	 <see cref="string"/>
	 
	*/
	String getRetrieveSql();

	/** 
	 关联表Join结构
	 {0}关联表表名，{1}当前表关联字段名，{2}关联表关联字段名
	 <see cref="string"/>
	 
	*/
	String getJoinTableName();

	String getInnerJoinTableName();
}
